<?php

namespace App\Repository;

use App\Entity\Timesolution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Timesolution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Timesolution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Timesolution[]    findAll()
 * @method Timesolution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimesolutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Timesolution::class);
    }

    // /**
    //  * @return Timesolution[] Returns an array of Timesolution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Timesolution
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
