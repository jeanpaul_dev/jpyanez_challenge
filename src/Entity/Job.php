<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JobRepository")
 */
class Job
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $submitter;

    /**
     * @ORM\Column(type="integer")
     */
    private $processor;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\Column(type="datetime")
     */
    private $jobin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $jobout;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubmitter(): ?int
    {
        return $this->submitter;
    }

    public function setSubmitter(int $submitter): self
    {
        $this->submitter = $submitter;

        return $this;
    }

    public function getProcessor(): ?int
    {
        return $this->processor;
    }

    public function setProcessor(int $processor): self
    {
        $this->processor = $processor;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getJobin(): ?\DateTimeInterface
    {
        return $this->jobin;
    }

    public function setJobin(\DateTimeInterface $jobin): self
    {
        $this->jobin = $jobin;

        return $this;
    }

    public function getJobout(): ?\DateTimeInterface
    {
        return $this->jobout;
    }

    public function setJobout(?\DateTimeInterface $jobout): self
    {
        $this->jobout = $jobout;

        return $this;
    }
}
