<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Job;
use App\Entity\Timesolution;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Cache\ItemInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/job", name="job", methods={"POST"})
     */
    public function jobIn(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Job::class);
        $processor = $repository->findOneBy([
            'processor' => $_POST['processor'],
            'jobout' => null
        ]);
        if(!$processor){
            $entityManager = $this->getDoctrine()->getManager();
            $job = new Job();
            $job->setSubmitter($_POST['submitter']);
            $job->setProcessor($_POST['processor']);
            //1=low 2=middle 3=hi
            $job->setPriority($_POST['priority']);
            $job->setJobin(new \datetime());
            $entityManager->persist($job);

            $entityManager->flush();

            return $this->json([
                'message' => 'Job Inserted Succesfully',
                'id_job' => $job->getId()
            ]);
        }else{
            return $this->json([
            'message' => 'this processor is already working in a job'
            ]);
        }
    }

    /**
     * @Route("/job/{id}", name="job_show")
     */
    public function showJob($id)
    {
        $jobfound = $this->getDoctrine()
            ->getRepository(Job::class)
            ->find($id);

        if (!$jobfound) {
            throw $this->createNotFoundException(
                'No job found for id '.$id
            );
        }
        $outvalue = $jobfound->getJobout();
        if(!$outvalue){
            $state = 'non-completed';
        }else{
            $state = 'completed';
        }
        return $this->json([
            'in'=>$jobfound->getJobin(),
            'out'=>$jobfound->getJobout(),
            'processor'=>$jobfound->getProcessor(),
            'submitter'=>$jobfound->getSubmitter(),
            'priority'=>$jobfound->getPriority(),
            'state' => $state
        ]);

    }

    /**
     * @Route("/job/endjob/{id}")
     */
    public function endjob($id)
    {
        $now = new \datetime();
        $entityManager = $this->getDoctrine()->getManager();
        $job = $entityManager->getRepository(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException(
                'No job found for id '.$id
            );
        }else{

            $job->setJobout($now);
            $entityManager->flush();
        }    
        $repository = $this->getDoctrine()->getRepository(Timesolution::class);
        $sol = $repository->findOneBy([
            'job' => $id
        ]);

        if(!$sol){
            $solucion = new Timesolution();
     
            $solucion->setJob($id);
            $diff =  $now->getTimestamp() - $job->getJobin()->getTimestamp();
            //time in seconds
            $solucion->setTime($diff);
            $entityManager->persist($solucion);
            $entityManager->flush();
            //id 1 have the sum, the incremente started in 21 in this dev.
            $cache = $repository->findOneBy([
                'id' => 1
            ]);
            $prevtime = $cache->getTime();
            $cache->setTime($prevtime + $diff);
            $cache->setJob($solucion->getId());
            $entityManager->persist($cache);
            $entityManager->flush();
        }

        return $this->json([
            'message' => 'this job is finished'
        ]);
    }
    /**
     * @Route("/maxjobpriority")
     */
    public function maxjobpriority(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Job::class);
        //by default get older jobin date
        $job = $repository->findOneBy([
            'jobout' => null,
            'priority' => 3
        ]);
        if (!$job) {
            $job = $repository->findOneBy([
            'jobout' => null,
            'priority' => 2
            ]);
            if (!$job) {
                $job = $repository->findOneBy([
                'jobout' => null,
                'priority' => 1
                ]);
                if (!$job) {
                    return $this->json([
                        'message' => 'there is no non-completed job now'
                    ]);
                }else{
                    return $this->json([
                        'job'=> $job->getJobin(),
                        'submitter' => $job->getSubmitter(),
                        'processor' => $job->getProcessor(),
                        'priority' => $job->getPriority(),
                        'id' => $job->getId()
                        ]);      
                }
            }else{
                return $this->json([
                    'job'=> $job->getJobin(),
                    'submitter' => $job->getSubmitter(),
                    'processor' => $job->getProcessor(),
                    'priority' => $job->getPriority(),
                    'id' => $job->getId()
                    ]);
            }    
        }else{
            return $this->json([
                'jobIn'=> $job->getJobin(),
                'submitter' => $job->getSubmitter(),
                'processor' => $job->getProcessor(),
                'priority' => $job->getPriority(),
                'id' => $job->getId()
            ]);
        }
    }
     /**
     * @Route("/average", name="average")
     */
    public function average()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Timesolution::class);
        
        $cache = $repository->findOneBy([
                'id' => 1
        ]);
        $prevtime = $cache->getTime();
        $time = $cache->getTime();
        $numberjobfinished = $cache->getJob();
        $entityManager->persist($cache);
        $entityManager->flush();
        return $this->json([
            'avg' => round(($time / $numberjobfinished))." seconds"
        ]);

    }
}
